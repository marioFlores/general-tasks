import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route, Switch } from 'react-router-dom';
import SnackBar from "../components/ui/SnackBar";
import Register from "../containers/Auth/Register";
import Home from "../containers/Main/Home";
import Login from "../containers/Auth/Login";
import { IStore } from "../redux/types";
import Main from "../components/task/Main"
import PrivateRoute from "../components/private-route";

function Routes(): JSX.Element {

  const { currentUser, isLoading } = useSelector((state: IStore) => state.auth);

  return (
    <>
      <div className="App" style={{backgroundColor: 'cadetblue'}}>
        <Switch>
          <Route exa path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/tasks/home" component={Main} />
          <PrivateRoute isLoggedIn={!!currentUser} path="/demo" component={Home}/>
          
          <Route>
            <Redirect to="/demo" />
          </Route>
        </Switch>          
      </div>
      <SnackBar position={{ vertical: 'bottom', horizontal: 'left' }} duration={3000}/>
    </>
  );
}

export default Routes;
