import { Dispatch } from "react";
import Api from '../../../api/ApiService';
import * as types from './types'
import { Task, TaskCreate, TaskUpdate } from '../../types';
import { setSnackBar } from "../ui/actions";

const createGetTasksByUser = () => {
    return {
        type: types.GET_TASKS_BY_USER
    }
}

const getTasksByUserSuccess = (data: Task[]) => {
    return {
        type: types.GET_TASKS_BY_USER_SUCCESS,
        payload: data
    }
}

export const getTasksByUser = (userId: number | undefined)  => async (dispatch: Dispatch<any>) => {
    try {
        dispatch(createGetTasksByUser());
        const res = await Api.get("/tasks/findByUser/"+userId);        
        dispatch(getTasksByUserSuccess(res.data))        
    } catch (error) {
        dispatch(catchRequestErr(error));
    }
}

const createUpdateTask = () => {
    return {
        type: types.UPDATE_TASK
    }
}

const updateTaskSuccess = (data: Task[]) => {
    return {
        type: types.UPDATE_TASK_SUCCESS,
        payload: data
    }
}

export const updateTask = (task: TaskUpdate | undefined, idUser: number | undefined) => async (dispatch: Dispatch<any>) => {
    try {
        dispatch(createUpdateTask());
        const res =  await Api.put("tasks/update/"+task?.id,{description: task?.description,
        id: task?.id,
        title: task?.title,
        to_date: task?.to_date,});
        dispatch(getTasksByUser(idUser));
    } catch (error) {
        console.log(error.message);
        dispatch(catchRequestErr(error));
    }
}

const createAddTask = () => {
    return {
        type: types.ADD_TASK
    }
}

const addTaskSuccess = (data: Task) => {
    return {
        type: types.ADD_TASK_SUCCESS,
        payload: data
    }
}

export const addTask = (task: TaskCreate) => async (dispatch: Dispatch<any>) => {
    try {
        dispatch(createAddTask());
        await Api.post("tasks/create", task);
        dispatch(getTasksByUser(task.id_user));
        //dispatch(addTaskSuccess(res.data));
    } catch (error) {
        dispatch(catchRequestErr(error));
    }
}

const createChangeStatusTask = () => {
    return {
        type: types.CHANGE_STATUS_TASK
    }
}

const changeStatusTaskSuccess = (data: Task) => {
    return {
        type: types.CHANGE_STATUS_TASK_SUCCESS,
        payload: data
    }
}

export const changeStatusTask = (idTask: string, idUser: number | undefined) => async (dispatch: Dispatch<any>) => {    
    try {
        dispatch(createChangeStatusTask());
        await Api.put('tasks/changeStatus/'+idTask);
        dispatch(getTasksByUser(idUser))
    } catch (error) {
        dispatch(catchRequestErr(error));
    }
}

const createDeleteTask = () => {
    return {
        type: types.DELETE_TASK
    }
}

const DeleteTask = (data: Task) => {
    return {
        type: types.DELETE_TASK_SUCCESS,
        payload: data
    }
}

export const deleteTask = (id: string, idUser: number | undefined) =>  async (dispatch: Dispatch<any>) => {
    try {
        dispatch(createDeleteTask());
        await Api.delete("tasks/delete/"+id);
        dispatch(getTasksByUser(idUser));
    } catch (error) {
        dispatch(catchRequestErr(error));
    }
}

const catchRequestErr = (err: any) => (dispatch: Dispatch<any>) => {
    dispatch({
      type: types.REQUEST_FAILURE,
      payload: err.message
    });    
    setSnackBar({ type: 'error', msg: err.message });
};

export const clearTasks = () => {
    return {
      type: types.CLEAR_TASKS
    };
};