import * as types from './types';
import { UserLogin, UserRegister } from '../../types';
import { Dispatch } from 'react';
import * as authAPI from '../../../api/auth';
import { setSnackBar } from '../ui/actions';

const createRegister = () => {
  return {
    type: types.REGISTER
  };
}; 

const createLogin = () => {
  return {
    type: types.LOGIN
  }; 
};

const registerSuccess = (data: any) => {
  return {
    type: types.REGISTER_SUCCESS,
    payload: data
  };
};

const loginSuccess = (data: any) => {
  return {
    type: types.LOGIN_SUCCESS,
    payload: data
  }; 
};

const catchAuthRequestErr = (err: any) => (dispatch: Dispatch<any>) => {
  dispatch({
    type: types.AUTH_REQUEST_FAILURE,
    payload: err.message
  });
  console.log(err);
  dispatch(setSnackBar({ type: 'error', msg: err.message }));
};

export const login = (user: UserLogin, history: any) => async (
  dispatch: Dispatch<any>
) => {
  try {
    dispatch(createLogin());
    const res = await authAPI.loginUser(user);
    dispatch(loginSuccess(res.data));
    history.push('/tasks/home');
  } catch (err) {
    dispatch(catchAuthRequestErr(err));
  }
};

export const register = (user: UserRegister, history: any) => async (dispatch: Dispatch<any>) => {
  try {
    dispatch(createRegister());
    const res = await authAPI.registerUser(user);
    dispatch(registerSuccess(res.data));
    history.push('/login');
  } catch (err) {
    dispatch(catchAuthRequestErr(err));
  }
};

export const userLoggedOut = () => (dispatch: Dispatch<any>) => {
  localStorage.removeItem(process.env.REACT_APP_LOCAL_TOKEN as string);
  dispatch({
    type: types.LOGOUT
  }); 
};