export const REGISTER = 'REGISTER';
export const LOGIN = 'LOGIN';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export const AUTH_REQUEST_FAILURE = 'AUTH_REQUEST_FAILURE';
export const LOGOUT = 'LOGOUT';