import * as types from '../actions/tasks/types';
import { Action, ITaskState } from '../types';


const initialState: ITaskState = {
    tasks: [],
    isLoading: true,
    err: null
  };
  
  export const taskReducer = (state = initialState, action: Action): ITaskState => {
    switch (action.type) {
      case types.GET_TASKS_BY_USER:
        return {
          ...state,
          isLoading: true
        };
      case types.UPDATE_TASK:
        return {
          ...state,
          isLoading: true
      };
      case types.ADD_TASK:
        return{
          ...state,
          isLoading: true
        };
      case types.DELETE_TASK:
        return {
          ...state,
          isLoading: true
        };
      case types.CHANGE_STATUS_TASK:
        return {
          ...state,
          isLoading: true
        }
      case types.REQUEST_FAILURE:
        return {
          ...state,
          isLoading: false,
          err: action.payload
        };
      case types.GET_TASKS_BY_USER_SUCCESS:
        return {
          ...state,
          tasks: action.payload,
          isLoading: false,
          err: null
        };
      case types.UPDATE_TASK_SUCCESS:{
        const tasks = state.tasks.map((task) => {
          if (task.id === action.payload.id)
            task.title = action.payload.title;
            task.description = action.payload.description;
            task.to_date = action.payload.to_date;            
          return task;
        });
        return {
          ...state,
          tasks,
          isLoading: false,
          err: null
        };
      }
      case types.ADD_TASK_SUCCESS:
        return{
          ...state,
          tasks: action.payload,
          isLoading: false,
          err: null
        }
        /* return {
          ...state,
          tasks: action.payload,
          isLoading: false,
          err: null
        }; */
      case types.CLEAR_TASKS:
          return {
            tasks: [],
            isLoading: true,
            err: null
          }; 
      default:
        return state;
    }
  };