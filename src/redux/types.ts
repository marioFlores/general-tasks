export type UserLogin = {
  username: String;
  password: String;
};

export type UserRegister = {
  name: String;
  last_name: String;
  username: String;
  password: String;
  email: String;
  type: String;
}

export type Task = {
  id: string,
  id_user: number,
  date_create: Date,
  title: string,
  description: string,
  status: boolean,
  to_date: string
}

export type TaskUpdate = {
  id?: string | undefined, 
  title?: string | undefined,
  description?: string | undefined,  
  to_date?: string| undefined
}

export type TaskCreate = {
  title: string,
  description: string,
  to_date: string,
  id_user: number | undefined
}

export interface ITaskState {
  tasks: Task[];
  isLoading: boolean;
  err: any;
}

export type CurrentUser = {  
  token?: string;
  user: {
    id: number;
    name: String;
    last_name: String;
    createdOn: Date;
    email: string;
  }
};

export type AlertType = 'success' | 'info' | 'warning' | 'error' | undefined;

export type SnackBarAlert = {
  type: AlertType;
  msg: string;
};

export type Action = {
  type: string;
  payload: any;
};

export interface IStore {
  ui: IUiState;
  auth: IAuth;
  task: ITaskState;
}

export interface IUiState {
  snackbar: SnackBarAlert;
}

export interface IAuth {
  currentUser: CurrentUser | null;
  err: any;
  isLoading: boolean;
}
