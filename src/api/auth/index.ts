import { UserLogin, UserRegister } from '../../redux/types';
import * as utils from '../../utils/index';
import Api from '../ApiService';

export const registerUser = async (user: UserRegister) => {
  try {
    const res = await Api.post('auth/register', user);

    utils.setAxiosAuthToken(res.data.token);
    utils.setLocalStorageToken(res.data.token);

    return res;
  } catch (err) {
    throw new Error(err.response?.data.message);
  }
};

export const loginUser = async (userLogin: UserLogin) => {
  try {
    const res = await Api.post("auth/login", userLogin);

    utils.setAxiosAuthToken(res.data.token);
    utils.setLocalStorageToken(res.data.token);

    return res;
  } catch (err) {
    throw new Error(err.response?.data.message);
  }
};