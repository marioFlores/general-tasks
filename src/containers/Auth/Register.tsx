import React, { useState, useEffect } from 'react';
import { TextField, Button } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import { UserRegister } from '../../redux/types';
import { useDispatch } from 'react-redux';
import * as authActions from '../../redux/actions/auth/actions';
import { makeStyles } from '@material-ui/core';
import { Container } from '@material-ui/core';
import { Card } from '@material-ui/core';
import { CardContent } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import { CardActions } from '@material-ui/core';
import { InputLabel } from '@material-ui/core';
import { Select } from '@material-ui/core';
import { MenuItem } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
      maxWidth: 345,
      width: '500px',
    },
    container:{          
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'cadetblue',
        height: '100vh'
    },
    cardactions: {
        display: 'flex', 
        flexDirection: 'column'
    },
    cardActionsButto: {
        background: 'white',
        width: '100%',
        height: '40px',
    },
    cardContent: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: '0px'
    },
    divNewAccountPrincipal: {
        display: 'flex', 
        justifyContent: 'center',
        paddingBottom: '20px',
        paddingTop: '5px'
    },
    linkNewAccount:{
        textDecoration: 'none', 
        marginLeft: '0.5rem',        
    }

});

const Register = () => {
    const classes = useStyles();
    const [user, setUser] = useState<UserRegister>({   
        name: '',
        last_name: '',
        username: '',
        password: '',
        email: '',
        type: "GENERAL"
    });

    const [userType, setUserType] = React.useState('GENERAL');

    const dispatch = useDispatch();
    const hisotry = useHistory();  
    
    const onChangeHandler = ( e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { value, id } = e.target;
        console.log(value);
        setUser((prevState) => ({
            ...prevState,
            [id]: value
        }));
    };

    const onSubmitHandler = (e: React.FormEvent) => {
        e.preventDefault();
        dispatch(authActions.register(user, hisotry));
    };

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        console.log(user);
        setUserType(event.target.value as string);
        user.type = userType;
        /* setUser((prevState) => ({
            ...prevState,
            type: userType
        })); */
        console.log(user);
    };

  return (
    <Container fixed className={classes.container}>
       <Card className={classes.root}>
            <form onSubmit={onSubmitHandler}>
                <CardContent className={classes.cardContent}>
                    <Typography style={{margin: '20px 0px'}} gutterBottom variant="h5" component="h2">
                        Register
                    </Typography>
                    
                    <TextField style={{marginBottom: '20px'}} autoFocus={true} id="name" label="Name" name="name" type="text" value={user.name} onChange={onChangeHandler} required/>
                    <TextField style={{marginBottom: '20px'}} id="last_name" label="Last Name" name="last_name" type="text" value={user.last_name} onChange={onChangeHandler} required/>
                    <TextField style={{marginBottom: '20px'}} id="username" label="Username" name="username" type="text" value={user.username} onChange={onChangeHandler} required/>
                    <TextField style={{marginBottom: '20px'}} id="password" label="Password" name="password" type="password" value={user.password} onChange={onChangeHandler} required />
                    <TextField style={{marginBottom: '20px'}} id="email" label="Email" name="email" type="email" value={user.email} onChange={onChangeHandler} required/>
                    <InputLabel id="type" style={{width: '63%', marginTop: '20px'}}>Type User</InputLabel>
                    <Select style={{marginBottom: '10px', width: '63%'}}
                        labelId="type"
                        id="type"
                        name="type"
                        value={userType}
                        onChange={handleChange}
                        required                    
                        >
                            <MenuItem value="GENERAL">General</MenuItem>
                            <MenuItem value="ADMIN">Admin</MenuItem>                    
                        </Select>                 
                
                </CardContent>
                <CardActions className={classes.cardactions}>
                    <Button  type="submit" size="small" color="primary" className={classes.cardActionsButto}>
                        Sign Up
                    </Button>                
                </CardActions>
            </form>
            <div className={classes.divNewAccountPrincipal}>
                <div>Already have an account?</div>
                <div>
                    <Link className={classes.linkNewAccount} to="/login">
                        Login
                    </Link>
                </div>
            </div>
       </Card>
    </Container>
  );    
};

export default Register;