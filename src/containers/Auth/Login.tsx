import React, { useState, useEffect } from 'react';
import { TextField, Button, makeStyles, Card } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import * as authActions from '../../redux/actions/auth/actions';
import { CardContent } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import { CardActions } from '@material-ui/core';
import { Container } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
      maxWidth: 345,
      width: '500px',
    },
    container:{
        height: "100vh",       
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    cardactions: {
        display: 'flex', 
        flexDirection: 'column'
    },
    cardActionsButto: {
        background: 'white',
        width: '100%',
        height: '40px',
    },
    cardContent: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: '0px'
    },
    divNewAccountPrincipal: {
        display: 'flex', 
        justifyContent: 'center',
        paddingBottom: '20px',
        paddingTop: '5px'
    },
    linkNewAccount:{
        textDecoration: 'none', 
        marginLeft: '0.5rem',        
    }

});

interface IUser{
    username: String,
    password: String
}

const Login = () => {
    const classes = useStyles();

  const [user, setUser] = useState<IUser>({ 
    username: '',
    password: ''
  });

  const dispatch = useDispatch();
  const history = useHistory();

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { value, id } = e.target;

    setUser((prevState: IUser) => ({
      ...prevState,
      [id]: value
    }));
  };

  const onSubmitHandler = (e: React.FormEvent) => {
    e.preventDefault();
    dispatch(authActions.login(user, history));
  };

  return (
    <Container fixed className={classes.container}>
        <Card className={classes.root}>
            <form onSubmit={onSubmitHandler}>
                <CardContent className={classes.cardContent}>
                    <Typography style={{margin: '20px 0px'}} gutterBottom variant="h5" component="h2">
                        Log In
                    </Typography>
                    
                    <TextField style={{marginBottom: '20px'}} autoFocus={true} id="username" label="Username" name="username" type="text" value={user.username} onChange={onChangeHandler} required/>
                    <TextField style={{marginBottom: '20px'}} id="password" label="Password" name="password" type="password" value={user.password} onChange={onChangeHandler} required />
                    
                </CardContent>
                <CardActions className={classes.cardactions}>
                    <Button  type="submit" size="small" color="primary" className={classes.cardActionsButto}>
                        Login
                    </Button>                
                </CardActions>
            </form>
            <div className={classes.divNewAccountPrincipal}>
                <div>New User?</div>
                <div>
                    <Link className={classes.linkNewAccount} to="/register">
                        Create an account
                    </Link>
                </div>
            </div>
        </Card>
    </Container>
    
  );
};

export default Login;