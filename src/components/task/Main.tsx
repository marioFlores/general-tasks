import React, { useEffect } from "react";
import {
  makeStyles,
  Theme,
  createStyles,
} from "@material-ui/core/styles";
import { SectionToDo } from './SectionToDo';
import AppBar from "@material-ui/core/AppBar";
import { IconButton } from "@material-ui/core";
import { Typography } from "@material-ui/core";
import { Button } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { userLoggedOut } from "../../redux/actions/auth/actions";
import { IStore } from "../../redux/types";
import { useHistory } from "react-router-dom";
import { getTasksByUser } from '../../redux/actions/tasks/actions';
import { Toolbar } from "@material-ui/core";
import { SectionDone } from "./SectionDone";
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    principalToDo: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      textAlign: 'center',
      height: 'auto',
      backgroundColor: '#fff',
    },
  })  
);

const Main = () => {  
  const classes = useStyles();
  const authState = useSelector((state: IStore) => state.auth); 
  const dispatch = useDispatch();
  const history = useHistory();

  const getTasks = () =>{    
    dispatch(getTasksByUser(authState.currentUser?.user.id))
  }

  useEffect(() => {
    if(authState.currentUser?.user.id){
      getTasks();
    }else{
      history.push("/login");
    }    
  }, [])  

  const logoutHandler = () => {
    dispatch(userLoggedOut());
    history.push("/login"); 
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <AssignmentIndIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Welcome {authState.currentUser?.user.name}
          </Typography>
          <Button color="inherit" onClick={logoutHandler}>            
              Log out            
          </Button>
        </Toolbar>
      </AppBar>
      <div className={classes.principalToDo}>        
        <SectionToDo/>
        <SectionDone/>
      </div>      
    </div>
  );
}

export default Main;
