import React from 'react';
import { Typography } from '@material-ui/core';
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';
import { createStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { IStore } from '../../redux/types';
import { useHistory } from 'react-router-dom';
import { Theme } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { Fab } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import { Button } from '@material-ui/core';
import { deleteTask } from '../../redux/actions/tasks/actions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({  
    done:{
      width: '50%',
      backgroundColor: '#c6ffc4',
      margin: '10px',
      minHeight: '550px',
    },
    paper: {     
        width: '45%',
        height: '300px',
        backgroundColor: theme.palette.background.paper,      
        boxShadow: theme.shadows[5],
        float: 'left',
        margin: '10px',
    },
  })  
);

export const SectionDone = () => {
    const classes = useStyles();
    const authState = useSelector((state: IStore) => state.auth);
    const taskState = useSelector((state: IStore) => state.task);
    const dispatch = useDispatch();    
    const [open, setOpen] = React.useState(false);
    const [idTask, setIdTask] = React.useState('');
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleClickOpen = (id: string) => {
        setIdTask(id);
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setIdTask('');
    };

    const handleDeleteTask = () => {
        dispatch(deleteTask(idTask, authState.currentUser?.user.id));
        setOpen(false);
    }

    return (
        <div className={classes.done}>
            <Container maxWidth="sm">
                <Typography variant="h5" style={{margin: '10px 0px'}}>
                    Tasks Done
                </Typography>
                {
                    taskState.tasks.map(task =>(
                        task.status?
                            <Paper elevation={5} className={classes.paper} >
                    <div style={{margin: '15px 10px'}}>
                        <label>Title: </label>
                        <input type="textarea" style={{marginTop: '6px'}} value={task.title} disabled/>
                    </div>
                    <div style={{margin: '15px 10px'}}>
                        <label>Description:</label>
                        <textarea name="textarea" value={task.description} rows={5} cols={30} style={{resize: "none"}} disabled>{task.description}</textarea>
                    </div>
                    <div style={{margin: '15px 10px'}}>
                    <TextField
                        id="to_date"
                        label="to date"
                        type="date"
                        defaultValue={task.to_date.substring(0,10)}                      
                        InputLabelProps={{
                            shrink: true,
                        }}
                        disabled
                    />
                    </div>
                    <div style={{margin: '15px 10px'}}>                        
                        <Fab color="secondary" size="small" aria-label="edit" style={{ marginRight: '5px' }} onClick={(e) => handleClickOpen(task.id)} >
                            <DeleteIcon />
                        </Fab>
                    </div>                        
                    </Paper> :
                    <div></div>
                    ))
                }  
            </Container>
            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                disableBackdropClick
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{"Delete this task?"}</DialogTitle>
                
                <DialogActions>
                <Button autoFocus onClick={handleClose} color="primary">
                    Disagree
                </Button>
                <Button onClick={handleDeleteTask} color="secondary" autoFocus>
                    Agree
                </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}