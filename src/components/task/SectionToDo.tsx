import Fab from "@material-ui/core/Fab";
import Paper from "@material-ui/core/Paper";
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DoneIcon from '@material-ui/icons/Done';
import { green } from '@material-ui/core/colors';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import React from "react";
import TextField from "@material-ui/core/TextField";
import { Container } from "@material-ui/core";
import { Typography } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { IStore } from "../../redux/types";
import { addTask, changeStatusTask, updateTask } from "../../redux/actions/tasks/actions";
import { Modal } from "@material-ui/core";
import { Snackbar } from "@material-ui/core";
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';

function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 45 + rand();
    const left = 45 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toDo: {
        width: '50%',
        backgroundColor: '#f1f1fb',
        margin: '10px', 
        minHeight: '550px',
    },
    paper: {     
        width: '45%',
        height: '300px',
        backgroundColor: theme.palette.background.paper,      
        boxShadow: theme.shadows[5],
        float: 'left',
        margin: '10px',
    },
    modal: {
        position: 'absolute',
        borderRadius: '10px 10px',
        width: 400,
        backgroundColor: theme.palette.background.paper,      
        padding: theme.spacing(1, 3, 2),
    },
    modalPaper: {
        textAlign: 'center',
    },
    modalUpdateTask: {
        textAlign: 'center',

    },
    modalPaperElements:{
        margin: '15px 10px', 
        display: 'inline-grid',
    }
  })
);

export interface ITask{
    id?: string,
    id_user?: number,
    date_create?: Date,
    title?: string,
    description?: string,
    status?: boolean,
    to_date?: string
}

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export const SectionToDo = () => {
    const classes = useStyles();
    const authState = useSelector((state: IStore) => state.auth);
    const taskState = useSelector((state: IStore) => state.task)
    const dispatch = useDispatch();

    const [modalStyle] = React.useState(getModalStyle);
    const [openModalCreate, setOpenModalCreate] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const [taskModal, setTaskModal] = React.useState<ITask>();    
    const [title,setTitle] = React.useState("");
    const [description,setDescription] = React.useState("");
    const [date,setDate] = React.useState("");    
    const [error, setError] = React.useState("");
    const [openBar, setOpenBar] = React.useState(false);

    const handleClick = () => {
        setOpenBar(true);
    };

    const handleCloseBar = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
        return;
        }

        setOpenBar(false);
    };

    const handleOpen = (e:any, id: string) => { 
        const addTask = taskState.tasks.find(task => id == id);    
        setTaskModal(addTask);    
        setOpen(true);
    };

    const handleClose = () => {
        setTitle('');
        setDescription('');
        setDate('');
        setOpen(false);
    };    

    const handleOpenCreateTask = () => {    
        setOpenModalCreate(true);
    }

    const handleCloseCreateTask = () => {        
        setTitle('');
        setDescription('');
        setDate('');
        setOpenModalCreate(false);
    }; 

    const handleEditTask = () => {      
        dispatch(updateTask({
            id: taskModal?.id,
            title: !title? taskModal?.title:title,
            description: !description?taskModal?.description:description,
            to_date: !date?taskModal?.to_date:date
        },authState.currentUser?.user.id));

        setOpen(false);

        setTitle('');
        setDescription('');
        setDate('');
        
    }

    const handleCreateTask = () => {

        if(!title){
            setError("Please add title");
            setOpenBar(true);
            return
        }        

        if(!description){
            setError("Please add description");
            setOpenBar(true);
            return
        }

        if(!date){
            setError("Please add to date");
            setOpenBar(true);
            return
        }        
        
        dispatch(addTask({
            title: title,
            description: description,
            to_date: date,
            id_user: authState.currentUser?.user.id
        }));

        setOpenModalCreate(false);

        setTitle('');
        setDescription('');
        setDate('');
              
    }

    const handleTitleChange = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) =>{
        setTitle(value);
    }

    const handleDescriptionChange = ({ target: { value } }: React.ChangeEvent<HTMLTextAreaElement>) => {
        setDescription(value);
    };

    const handleDateChange = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
        setDate(value);
    };
    
    const handleChangeStatus = (e:any, id: string) =>{        
        dispatch(changeStatusTask(id, authState.currentUser?.user.id));
    }
    

    const body = (
        <div style={modalStyle} className={classes.modal}>
          <h2 id="simple-modal-title">Edit task</h2>          
          <div className={classes.modalPaper}>
            <div className={classes.modalPaperElements}>
                <label>Title: </label>
                <input type="textarea" id="title" style={{marginTop: '6px'}} defaultValue={taskModal?.title} onChange={handleTitleChange}/>
            </div>
            <div className={classes.modalPaperElements}>
                <label style={{marginBottom: '10px'}} >Description:</label>
                <textarea name="textarea" id="description" rows={5} cols={30} style={{resize: "none"}} onChange={handleDescriptionChange}>{taskModal?.description}</textarea>
            </div>
            <div className={classes.modalPaperElements}>
            <TextField
                id="to_date"
                label="to date"
                type="date"
                defaultValue={taskModal?.to_date?.substring(0,10)}
                onChange={handleDateChange}
                InputLabelProps={{
                  shrink: true,
                }}            
            />
            </div>
            <div style={{margin: '15px 10px'}}>           
    
                <Fab aria-label="edit" size="small" style={{ color: green[500],marginRight: '15px' }} onClick={handleEditTask}>
                    <SaveIcon />
                </Fab>
    
                <Fab color="secondary" size="small" aria-label="edit" style={{ marginRight: '5px' }} onClick={handleClose}>
                    <CloseIcon />
                </Fab>
            </div>
          </div>           
        </div>
    );

    const bodyCreate = (
        <div style={modalStyle} className={classes.modal}>
          <h2 id="simple-modal-title" style={{textAlign: 'center'}}>Create a task</h2>

          <div className={classes.modalPaper}>
            <div className={classes.modalPaperElements}>
                <label style={{marginBottom: '10px'}}>Title: </label>
                <input type="textarea" id="title" style={{marginTop: '6px'}} onChange={handleTitleChange} required/>
            </div>
            <div className={classes.modalPaperElements}>
                <label>Description:</label>
                <textarea name="textarea" id="description" rows={5} cols={30} style={{resize: "none"}} onChange={handleDescriptionChange} required></textarea>
            </div>
            <div className={classes.modalPaperElements}>
            <TextField
                id="to_date"
                label="to date"
                type="date"               
                onChange={handleDateChange}
                required
                InputLabelProps={{
                  shrink: true,
                }}            
            />
            </div>
            <div style={{margin: '15px 10px'}}>           
    
                <Fab aria-label="edit" size="small" style={{ color: green[500],marginRight: '15px' }} onClick={handleCreateTask}>
                    <SaveIcon />
                </Fab>
    
                <Fab color="secondary" size="small" aria-label="edit" style={{ marginRight: '5px' }} onClick={handleCloseCreateTask}>
                    <CloseIcon />
                </Fab>
            </div>
          </div>           
        </div>
    );

    return (
        <div className={classes.toDo}>                 
          <Container maxWidth="sm">
            <Typography variant="h5" style={{margin: '10px 0px'}}>
              To Do
            </Typography>
            <div>
                <Fab color="primary" size="small" aria-label="add" onClick={handleOpenCreateTask}>
                    <AddIcon />
                </Fab>
            </div>
            
            {
              taskState.tasks.map(task =>(
                !task.status?
                <Paper elevation={5} className={classes.paper} >
                  <div style={{margin: '15px 10px'}}>
                      <label>Title: </label>
                      <input type="textarea" style={{marginTop: '6px'}} value={task.title} disabled/>
                  </div>
                  <div style={{margin: '15px 10px'}}>
                      <label>Description:</label>
                      <textarea name="textarea" value={task.description} rows={5} cols={30} style={{resize: "none"}} disabled>{task.description}</textarea>
                  </div>
                  <div style={{margin: '15px 10px'}}>
                  <TextField
                      id="to_date"
                      label="to date"
                      type="date"
                      defaultValue={task.to_date.substring(0,10)}                      
                      InputLabelProps={{
                        shrink: true,
                      }}
                      disabled
                  />
                  </div>
                  <div style={{margin: '15px 10px'}}>
                      <Fab aria-label="edit" size="small" style={{ color: green[500],marginRight: '15px' }} onClick={(e) => handleChangeStatus(e,task.id)}>
                          <DoneIcon />
                      </Fab>                      

                      <Fab color="secondary" size="small" aria-label="edit" style={{ marginRight: '5px' }} onClick={(e) => handleOpen(e, task.id)}>
                          <EditIcon />
                      </Fab>
                  </div>                        
                </Paper> :
                <div></div>
              ))
            }                   
          </Container>
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"        
            disableBackdropClick
            className={classes.modalUpdateTask}
            >
                {body}
            </Modal>
            <Modal
                open={openModalCreate}
                onClose={handleCloseCreateTask}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"        
                disableBackdropClick
            >
                {bodyCreate}
            </Modal>
            <Snackbar open={openBar} autoHideDuration={3000} anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }} onClose={handleCloseBar}>
                <Alert onClose={handleCloseBar} severity="error">
                {error}
                </Alert>
            </Snackbar>            
        </div>
    )

}